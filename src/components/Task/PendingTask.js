import React from 'react'
import "./style.css"


function Task(props) {

  function deleteItem(id) {
    props.removeFromPendingTasks(id);
  }

  function markRead(id) {
    props.addInDoneTask(id);
    props.removeFromPendingTasks(id);
  }

  return (
    <div>
      <div class="task">
        <div class="taskdiv">
          <h3>{props.item.value}</h3>
          <p>
            Created On: {props.item.createdOn}
          </p>
        </div>
        <div class="action">
          <button class="btn btn-primary com" onClick={() => markRead(props.item.id)}>Mark As Done</button>
          <button class="btn" onClick={() => deleteItem(props.item.id)}><i class="fa fa-times fa-2x" aria-hidden="true"></i></button>
        </div>
      </div>
    </div>
  )
}

export default Task;
