import React, { useState } from 'react'
import "./style.css"

var a = 1;

function TaskForm(props) {

    function addItems(){
      if(!props.newTask){
          alert("Task name may not be empty");
          return;
      }
      var date = new Date();
      const item = {
          id: a++,
          value: props.newTask,
          createdOn: date.toUTCString()
      };

      props.setPendingTask(oldlist => [...oldlist, item]);
      props.setnewTask("");
   }
  return (
    <div>
       <div>
            <div>
                <h1>What To do Today ?</h1>
            </div>
            <div>
                <textarea class="textarea" id="desc" rows="3" 
                placeholder="Enter your task here" 
                value={props.newTask}
                onChange = {e => props.setnewTask(e.target.value)}
                ></textarea>
                <button type="submit" id="add" class="btn" onClick={()=> addItems()}>Save</button>
            </div>
        </div>
    </div>
  )
}

export default TaskForm
