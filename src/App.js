import './App.css';
import PendingTask from "./components/Task/PendingTask";
import DoneTask from './components/DoneTask/DoneTask';
import TaskForm from './components/TaskForm/TaskForm';
import React, { useState } from 'react';

function App() {

  const [newTask, setnewTask] = useState("");
  const [pendingTasks, setPendingTask] = useState([]);
  const [doneTasks, setDoneTask] = useState([]);

  const removeFromPendingTasks = (id) => {
    setPendingTask(pendingTasks.filter(item => item.id !== id));
  }

  const removeFromDoneTask = (id) => {
    setDoneTask(doneTasks.filter(item => item.id !== id));
  }

  const addInDoneTask = (id) => {
    const getTask = pendingTasks.filter(item => item.id === id)[0];
    setDoneTask(oldlist => [...oldlist, getTask]);
  }

  return (
    <div className="main">

      <TaskForm newTask={newTask} pendingTasks={pendingTasks} setnewTask={setnewTask} setPendingTask={setPendingTask} />
      <div id="main">
        <h3>{`Your Tasks (${pendingTasks.length})`}</h3>
        <div>
            {pendingTasks.length > 0 && pendingTasks.map(item => {
              return (
                <PendingTask item={item} removeFromPendingTasks={removeFromPendingTasks} addInDoneTask={addInDoneTask}/>
              )
            })}
        </div>
        <h3>{`Done Tasks (${doneTasks.length})`}</h3>
        <div>
            {doneTasks.length > 0 && doneTasks.map(item => {
              return (
                <DoneTask item={item} removeFromDoneTask={removeFromDoneTask}/>
              )
            })}
        </div>
      </div>
    </div>
  );
}

export default App;
